json.extract! admin_short_url, :id, :admin_news_id_id, :short_url_key, :created_at, :updated_at
json.url admin_short_url_url(admin_short_url, format: :json)
