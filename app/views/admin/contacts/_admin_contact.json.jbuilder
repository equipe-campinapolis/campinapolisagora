json.extract! admin_contact, :id, :name, :second_name, :email, :social, :message, :created_at, :updated_at
json.url admin_contact_url(admin_contact, format: :json)
