class CategoriaValidator
    include ActiveModel::Validations
    attr_reader :id
    def initialize(id=nil)
        @id = id
    end
    validates_inclusion_of :id, :in => %w( janeiro fevereiro março abril maio junho julho agosto setembro outrubro novembro ), :message => "você digitou um mês invalido" 
end 
    