class Admin::Newsletter < ApplicationRecord
    after_create :send_mail
    private
    def send_mail
      NewsletterMailer.with(mail: self).newsletter.deliver_now!
    end
end
