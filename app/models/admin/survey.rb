class Admin::Survey < ApplicationRecord
  before_create :only_one_row?

  default_scope {where(active: true)}
  def only_one_row?
    if (Admin::Survey.count == 1)
        errors.add(:base, "Só é Permitido um Unico Texto")
        raise ActiveRecord::RecordInvalid.new(self)
    end
  end
end
