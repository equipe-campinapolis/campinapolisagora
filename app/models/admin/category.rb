class Admin::Category < ApplicationRecord
	extend FriendlyId
	friendly_id :name, use: :slugged
	has_one :admin_news, class_name: "News", foreign_key: 'admin_category_id'
    #bug do frieldyid com rails_admin
	def slug=(value)
  		if value.present?
    		write_attribute(:slug, value)
   		end
 	end
end


