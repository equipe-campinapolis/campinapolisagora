class Admin::Advertisement < ApplicationRecord
  belongs_to :admin_publisher,class_name: "Publisher"
  has_one_attached :image #cloudinary + carrierwave
  enum link_status: [:sim, :não]

  validates :customer,presence: true,presence:{message:"Digite o nome de quem contratou o comercial"} 
  validates :expiration, presence: true 
  #validates :expiration, presence: true, if: :data_valida? 
  validates_date :expiration, :on_or_after => lambda { Date.current } 
  validates :image, presence: true

  #def data_valida? 
   #ano_corrente = Date.current.year 
   #ano_de_nascimento = self.nascimento.year 
   #if ano_corrente - ano_de_nascimento > 120 
   #  errors.add(:nascimento, "data de nascimento invalida!") 
   #  true 
   #end 
   #true 
  #end

  rails_admin do
    edit do
      configure :customer do
        partial "ad_customer"
      end
      configure :link do
        partial "ad_link"
      end
    end    
  end

end
