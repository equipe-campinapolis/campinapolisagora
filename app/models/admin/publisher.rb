class Admin::Publisher < ApplicationRecord
	extend FriendlyId
 	friendly_id :name, use: [:finders, :slugged]
 	belongs_to :admin_user,class_name: "User"
 	validates :name, presence: true, :allow_nil => false, :allow_blank => false

	#bug do frieldyid com rails_admin
    def slug=(value)
    	if value.present?
      		write_attribute(:slug, value)
    	end
  	end

end
