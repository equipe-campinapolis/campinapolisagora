class Admin::News < ApplicationRecord
  include Rails.application.routes.url_helpers
  extend FriendlyId
  after_create :encurta_url
  after_update :encurta_url
  friendly_id :title, use: [:finders, :slugged]
  has_one_attached :image #cloudinary + carrierwave
  has_attached_file :news_img #ckeditor + paperclip + s3
  acts_as_punchable
  belongs_to :admin_publisher,class_name: "Publisher"
  belongs_to :admin_category,class_name: "Category"
  has_many :admin_news_comments, class_name: "NewsComment", foreign_key: 'admin_news_id'
  validates :admin_publisher, presence: true,:allow_nil => true
  validates :title, presence: true,:allow_nil => true
  validates :subtitle, presence: true,:allow_nil => true
  validates :date, presence: true
  validates :image, presence: true

  scope :slider, -> {where(active: true).order(created_at: :DESC) }
  scope :mais_populares, -> { where(active: true) }
  scope :mais_populares, -> { (order created_at: :DESC).limit(5) }
  scope :mais_lidas, -> { (order created_at: :DESC).limit(2) }
  scope :mais_lida, -> { where(active: true) }
  scope :ultimas_noticias, -> { where(active: true).order(created_at: :DESC).limit(3) }
  scope :noticias_mega_menu, -> { where(active: true) }
  scope :noticias_mega_menu, -> { (order created_at: :DESC).limit(6)  }


  def encurta_url
    id = self.id
    bitly = Bitly.new("o_49fkcdratb", "R_ae922440389e25ce98ffd0c6fba337a9")
    u = bitly.shorten('https://campinapolisagora.com.br/noticia/'+self.slug)
    asu = Admin::ShortUrl.new
    asu.admin_news_id = id
    asu.short_url_key = u.user_hash
    asu.save
  end
 
  rails_admin do
    label "Notícia"
    label_plural "Notícias"
    edit do
      field :text,:ck_editor
      include_all_fields
      configure :title do
        label 'Titulo da Noticia: '
      end
      configure :date do
        label 'Data da Notícia: '
      end
      configure :subtitle do
        label 'Subtitulo da Notícia: '
      end
      configure :text do
        label 'Notícia: '
      end
      configure :active do
        label 'Noticia está visivel?: '
      end
      configure :admin_publisher do
        label 'Autor da Notícia'
      end
      configure :admin_category do
        label 'Categoria da Notícia'
      end
      configure :slug do
        hide
      end
      configure :source do
        label "Fonte"
      end
      configure :url do
        label "endereço da fonte"
      end
    end
  end

end
