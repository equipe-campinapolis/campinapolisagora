class Admin::User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  #devise :database_authenticatable, :registerable,
  #       :recoverable, :rememberable, :validatable, :omniauthable, :omniauth_providers => [:facebook]
  
         
  devise :database_authenticatable,
         :validatable, :omniauthable, :omniauth_providers => [:facebook]
  

         enum is_admin: [:autor, :admin]
  enum status: [:ativo, :inativo]
  validates :name, presence: true  

  def self.from_omniauth(auth)
    usuario = Admin::User.where(uid: auth[:uid]).first
    if not usuario.blank?
      usuario.token = auth.credentials.token
      usuario.save
    end
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.name = auth.info.name
      user.token = auth.credentials.token
      user.password = Devise.friendly_token[0,20]
      user.image = auth.info.image
    end
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end
  
end
