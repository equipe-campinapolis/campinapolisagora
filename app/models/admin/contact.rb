class Admin::Contact < ApplicationRecord
    validates :name,presence:{presence: true,message:"O seu Nome é Obrigatorio!"}
    validates :second_name,presence: false
    validates :email,presence:{presence: true,message:"O seu Email é Obrigatorio!"}
    validates :social,presence: false
    validates :message,presence:{presence: true,message:"Sua Mensagem é Obrigatoria!"}
end
