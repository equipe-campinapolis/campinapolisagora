class Admin::NewsComment < ApplicationRecord
  belongs_to :admin_news, class_name: 'News'
  belongs_to :admin_articles, class_name: 'Article'
  scope :inicio_comentarios, -> { (order created_at: :DESC).limit(5) } 
end