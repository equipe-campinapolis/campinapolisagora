class Admin::Article < ApplicationRecord
  extend FriendlyId
  friendly_id :title,use: :slugged
  acts_as_taggable_on :descritores
  has_attached_file :article_img
  belongs_to :admin_category, class_name: "Category"
  belongs_to :admin_publisher, class_name: "Publisher"
  has_many :admin_news_comments, class_name: "NewsComment", foreign_key: 'admin_news_id'

  #bug do frieldyid com rails_admin
  def slug=(value)
  	if value.present?
     		write_attribute(:slug, value)
   	end
  end
end
