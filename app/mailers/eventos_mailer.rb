class EventosMailer < ApplicationMailer
    default from: "boletim@campinapolisagora.com.br"

    def novo_evento
        email = params[:mail]
        mail to: email, subject: "Novo evento"
    end
end
