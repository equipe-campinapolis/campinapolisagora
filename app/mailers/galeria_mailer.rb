class GaleriaMailer < ApplicationMailer
    default from: "boletim@campinapolisagora.com.br"

    def nova_imagem
        email = params[:mail]
        mail to: email, subject: "Nova informacao"
    end
end
