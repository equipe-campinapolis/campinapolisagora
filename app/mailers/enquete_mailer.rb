class EnqueteMailer < ApplicationMailer
    default from: "boletim@campinapolisagora.com.br"

    def nova_enquete
        email = params[:mail]
        mail to: email, subject: "Nova enquete"
    end
end
