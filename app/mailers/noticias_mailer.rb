class NoticiasMailer < ApplicationMailer
    before_action :load_noticia
    default from: "boletim@campinapolisagora.com.br"

    def nova_noticia
        #email = params[:mail]
        mail to: @noticia.email, subject: "Nova noticia"
    end

    def load_noticia
        @noticia = params[:noticia]
    end

end
