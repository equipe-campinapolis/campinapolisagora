class ArtigosMailer < ApplicationMailer
    default from: "boletim@campinapolisagora.com.br"

    def novo_artigo
        email = params[:mail]
        mail to: email, subject: "novo artigo"
    end
end
