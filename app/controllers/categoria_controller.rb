class CategoriaController < ApplicationController
	include ApplicationHelper
	include NoticiasHelper
	def index
		@comentarios = component_tab_comentarios  
		if not params["categoria"].nil? then
			if params["categoria"] == "noticias" then
				categoria_id = Admin::Category.friendly.find(params["id"]).id
				@categoria_nome = Admin::Category.friendly.find(params["id"]).name
				@noticias = Admin::News.where(admin_category_id: categoria_id)
				@post = @noticias
				@noticias_cinco_mais_lidas = NoticiasHelper.mais_lidas
			end
			if params["categoria"] == "artigos" then
				categoria_id = Admin::Category.friendly.find(params["id"]).id
				@categoria_nome = Admin::Category.friendly.find(params["id"]).name
				@artigos = Admin::Article.where(admin_category_id: categoria_id)
				@post = @artigos
				@noticias_cinco_mais_lidas = NoticiasHelper.mais_lidas
			end
			if params["categoria"] == "arquivos" then
				if not params["id"].nil? then
					#valida se parametro é um mês
					id_validator = CategoriaValidator.new(params["id"].downcase)
					if not id_validator.valid? 
  						flash[:validators] = id_validator.errors.full_messages[0]
						redirect_to "/"
						return
					end 
					result = converte_data_string(params["id"]).titleize
					mes = Date::MONTHNAMES.index(result)
					@arquivos = Admin::News.where("EXTRACT(MONTH FROM created_at) = ?", mes)
				end
			end
		end
		#component tabs
		@comentarios = component_tab_comentarios
		#component archive
		@noticias_arquivadas = agrupar_noticias
	end

private

def component_tab_comentarios
	result = []
	comentarios = ComentariosHelper.inicio_comentarios
	comentarios.each do |comentario|
		usuario = Admin::User.where(uid: comentario.uid).first
		result << {:comentario => comentario,:usuario => usuario }
	end
	result
end


end
