class PagesController < ApplicationController
  include ApplicationHelper
  #before_action :configura_menu, :except => []

  def inicio
    configura_variaveis_do_menu
    configura_menu
  end

  def artigo
    configura_variaveis_do_menu
    configura_menu
  end

  
  def noticias
  end
  
  #def index_custom
  #end

  #categoria
  #def category
  #end

  def categoria
    configura_menu
    @view_categoria = params["categoria"]
    @view_id = params["id"]
    if not params["categoria"].nil? then
      if params["categoria"] == "noticias" then
        noticias = Admin::News.all
      end
     end
    if not params["categoria"].nil? then
      if params["categoria"] == "artigos" then
        artigos = Admin::Article.all
      end
    end
    @post ||= noticias
    @post ||= artigos
  end

  #noticia
  def noticia
     configura_menu
     if defined? (@@ip) then
       if request.ip != @@ip then
         @noticia.punch(request)
       end
     end
     if defined? (@@agent) then
       if md5(request.env["HTTP_USER_AGENT"]) != @@agent then
         @noticia.punch(request)
       end
     end
     @@ip = request.ip
     @@agent = md5(request.env["HTTP_USER_AGENT"])
   end
  

  #outras
  #def about
  #end

  #def contact
  #end

  #def login
  #end


private
  def configura_variaveis_do_menu
    @autores = Admin::Publisher.all
    @noticias = Admin::News.all
    @categorias = Admin::Category.all 
  end

  def configura_menu
    if params.has_key?(:action) then
      configura_menu_artigos if params["action"] == "artigo"
    end 
    #if(params.has_key?(:news_id))
    #    noticia_slug = params["news_id"]
    #    @noticia = Admin::News.friendly.find(noticia_slug)
    #    @comentarios = Admin::NewsComment.where(admin_news_id: @noticia.id)
    #    @numero_de_comentarios = Admin::NewsComment.where(admin_news_id: @noticia.id).count
    #  end
    #end
    @categorias_distintas_em_noticias = categorias_distintas_em_noticias
    @categorias_distintas_em_artigos = categorias_distintas_em_artigos
  end
  def configura_menu_artigos
    autor_slug = params["id"]
    autor_id = Admin::Publisher.friendly.find(autor_slug)
    @artigos = Admin::Article.where(admin_publisher_id:autor_id)
  end

end
