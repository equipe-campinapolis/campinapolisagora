class InicioController < ApplicationController
	include NoticiasHelper
	def index
		is_valid_token?
		@categorias_em_noticias = categorias_distintas("noticia")
		@categorias_em_artigos = categorias_distintas("artigo")
		@autores = Admin::Publisher.where(:id => Admin::Article.select(:admin_publisher_id).map(&:admin_publisher_id))
		@noticias_do_slider = noticias_do_slider
		@noticias_mega_menu = NoticiasHelper.noticias_mega_menu
		@noticias_mais_lidas = NoticiasHelper.mais_lidas
		@noticia_mais_lida = NoticiasHelper.noticia_mais_lida
		@noticias_cinco_mais_lidas = NoticiasHelper.mais_lidas
		@ultima_noticia = NoticiasHelper.ultima_noticia
		@ultimas_noticias = NoticiasHelper.ultimas_noticias
		@noticias_em_destaque = NoticiasHelper.noticias_em_destaque
		@comercial = Admin::Advertisement.limit(1).order("RANDOM()")
		@noticias_recentes = NoticiasHelper.noticias_recentes
		#component tabs
		#@comentarios = component_tab_comentarios
		#component archive
		@noticias_arquivadas = agrupar_noticias
	end


private

	def component_tab_comentarios
		result = []
		comentarios = ComentariosHelper.inicio_comentarios
		comentarios.each do |comentario|
			usuario = Admin::User.where(uid: comentario.uid).first
			result << {:comentario => comentario,:usuario => usuario }
		end
		result
	end

	def categorias_distintas (opt)
		result = []
		a = Admin::News.select(:admin_category_id).distinct if opt == "noticia"
		a = Admin::Article.select(:admin_category_id).distinct if opt == "artigo"
		a.each do |b|
    		result << Admin::Category.find(b["admin_category_id"])
    	end
    	result
	end


	def is_valid_token?
		if admin_user_signed_in?
			status = FacebookHelper.is_valid_token?(current_admin_user.token, '/me/books?fields=name,picture,written_by')
			if status == false
				flash[:token] = "facebook"
				#redirect_to root_path, alert: exception.fb_error_message
				#destroy_admin_user_session_path (destroy_admin_user_session_path)
				id = current_admin_user.id
				sign_out current_admin_user
				usuario = Admin::User.find(id)
				usuario.token = ""
				usuario.save
			end
		end
	end

end
