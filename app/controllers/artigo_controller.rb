class ArtigoController < ApplicationController
	def index
		if(params.has_key?(:id))
    		artigo_slug = params["id"]
    		@artigo = Admin::Article.friendly.find(artigo_slug)
    		@artigos = Admin::Article.all
		end
		@categorias_em_noticias = categorias_distintas("noticia")
		@categorias_em_artigos = categorias_distintas("artigo")
		@autores = Admin::Publisher.where(:id => Admin::Article.select(:admin_publisher_id).map(&:admin_publisher_id))
		@noticias = Admin::News.all
		@noticias_mega_menu = NoticiasHelper.noticias_mega_menu
	end

	def index_autor
		if(params.has_key?(:id))
    		autor_slug = params["id"]
    		autor = Admin::Publisher.friendly.find(autor_slug)
    		@artigos = Admin::Article.where(admin_publisher_id: autor.id)
		end
		@categorias_em_noticias = categorias_distintas("noticia")
		@categorias_em_artigos = categorias_distintas("artigo")
		@autores = Admin::Publisher.where(:id => Admin::Article.select(:admin_publisher_id).map(&:admin_publisher_id))
		@noticias = Admin::News.all
		@noticias_mega_menu = NoticiasHelper.noticias_mega_menu
		render "index"
	end
private
	def categorias_distintas (opt)
		result = []
		a = Admin::News.select(:admin_category_id).distinct if opt == "noticia"
		a = Admin::Article.select(:admin_category_id).distinct if opt == "artigo"
		a.each do |b| 
      		result << Admin::Category.find(b["admin_category_id"])
      	end 
      	result
	end
end
