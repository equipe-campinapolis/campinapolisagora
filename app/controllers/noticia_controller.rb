class NoticiaController < ApplicationController
	include ApplicationHelper
	def index
		set_contagem_visualizacoes
		if(params.has_key?(:id))
    		noticia_slug = params["id"]
    		@noticia = Admin::News.friendly.find(noticia_slug)
			@comentarios = Admin::NewsComment.where(admin_news_id: @noticia.id)
			@noticias_mega_menu = NoticiasHelper.noticias_mega_menu
		end
		@categorias_em_noticias = categorias_distintas("noticia")
		@categorias_em_artigos = categorias_distintas("artigo")
		@autores = Admin::Publisher.where(:id => Admin::Article.select(:admin_publisher_id).map(&:admin_publisher_id))
		@noticias = Admin::News.all
	end


private

	def categorias_distintas (opt)
		result = []
		a = Admin::News.select(:admin_category_id).distinct if opt == "noticia"
		a = Admin::Article.select(:admin_category_id).distinct if opt == "artigo"
		a.each do |b|
      		result << Admin::Category.find(b["admin_category_id"])
      	end
      	result
	end

	def set_contagem_visualizacoes
		session[:noticias_ids] = [] if session[:noticias_ids].nil? 
		if(params.has_key?(:id))
			gatilho = false
			if not session[:noticias_ids].nil? then
				session[:noticias_ids].each do |s|
					noticia = Admin::News.friendly.find(params["id"])
					if s == noticia.slug
						gatilho = true
						break
					end
				end
			end
			if gatilho == false then
				Admin::News.friendly.find(params["id"]).punch(request)
			end
			session[:noticias_ids] << Admin::News.friendly.find(params["id"]).slug
			session[:ip] = request.ip
			session[:agent] = md5(request.env["HTTP_USER_AGENT"])
		end
	end
end
