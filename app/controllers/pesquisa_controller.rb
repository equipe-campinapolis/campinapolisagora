class PesquisaController < ApplicationController
  include ApplicationHelper
	include NoticiasHelper
  def index
    q = params.permit(:q) if params.key?("q")
    @pesquisa = q["q"]
    @post_1 = Admin::News.where("title LIKE ?", "%#{@pesquisa}%")
    @post_2 = Admin::News.where("subtitle LIKE ?", "%#{@pesquisa}%")
    @post_3 = Admin::News.where("text LIKE ?", "%#{@pesquisa}%")
    @post_4 = Admin::Article.where("title LIKE ?", "%#{@pesquisa}%")
    @post_5 = Admin::Article.where("subtitle LIKE ?", "%#{@pesquisa}%")
    @post_6 = Admin::Article.where("article LIKE ?", "%#{@pesquisa}%")
    @post = []
    if @post_1.count > 0
      @post_1.each do |post| 
        @post << post
      end
    end
    if @post_2.count > 0 
      @post_2.each do |post|
        @post << post
      end
    end
    if @post_3.count > 0
      @post_3.each do |post|
        @post << post
      end
    end
    if @post_4.count > 0
      @post_4.each do |post|
        @post << post
      end
    end
    if @post_5.count > 0
      @post_5.each do |post|
        @post << post
      end
    end
    if @post_6.count > 0
      @post_6.each do |post|
        @post << post
      end
    end
    @post = @post.uniq.shuffle
  end

  def ultimas_noticias
    @ultimas_noticias = Admin::News.all.order('admin_news.created_at DESC')
  end

  def todas_as_noticias
    @todas_as_noticias = Admin::News.all.shuffle
  end

  def mais_lidas
    @mais_lidas = Admin::News.most_hit(365.day.ago,Admin::News.count)
  end

  def videos
    @videos = Admin::Videos.all
  end

  def eventos
    @eventos = Admin::Event.all
  end

end
