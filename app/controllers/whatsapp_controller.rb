class WhatsappController < ApplicationController
  respond_to :html
  def ultima_noticia
    a = Admin::ShortUrl.where(admin_news_id: Admin::News.last.id)
    b = root_url+"noticia/"+a[0].short_url_key if a.count > 0
    b = "https://bit.ly/"+a[0].short_url_key if a.count > 0
    render html: b
  end

  def noticia
    id = params.permit(:id)
    id = id[:id].to_i
    a = Admin::ShortUrl.where(admin_news_id: id)
    b = "https://bit.ly/"+a[0].short_url_key if a.count > 0
    render html: b
  end
end
