class VideoController < ApplicationController
  include EventosHelper
	def index
		@noticias_mega_menu = NoticiasHelper.noticias_mega_menu
		@categorias_em_noticias = categorias_distintas("noticia")
		@categorias_em_artigos = categorias_distintas("artigo")
		@autores = Admin::Publisher.where(:id => Admin::Article.select(:admin_publisher_id).map(&:admin_publisher_id))
		@noticias = Admin::News.all
		@eventos = Admin::Event.all
		@comercial = Admin::Advertisement.limit(1).order("RANDOM()")
	end


private

	def categorias_distintas (opt)
		result = []
		a = Admin::News.select(:admin_category_id).distinct if opt == "noticia"
		a = Admin::Article.select(:admin_category_id).distinct if opt == "artigo"
		a.each do |b|
    	result << Admin::Category.find(b["admin_category_id"])
    end
    result
	end
end
