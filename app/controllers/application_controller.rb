require "securerandom"
require 'json'
class ApplicationController < ActionController::Base
  include Pundit
  include ComentariosHelper
  include InicioHelper
  before_action :set_session
  before_action :set_comentarios
  before_action :set_principais
  before_action :set_membros
  protect_from_forgery with: :exception
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized 
    
  def set_membros
    @membros = Admin::Publisher.all
  end

  def set_comentarios
    @comentarios_rodape = comentarios_rodape
  end

  def set_principais
    @principais = get_principais
  end

  def set_session
    session[:user_id] ||= SecureRandom.uuid
    cookies.permanent.signed[:user_id] ||= session[:user_id]
  end

  def current_user
    @current_user ||= Admin::User.first
  end

  def can_administer?
    current_user.try(:is_admin?)
  end

  def self.categorias_distintas (opt)
		result = []
		a = Admin::News.select(:admin_category_id).distinct if opt == "noticia"
		a = Admin::Article.select(:admin_category_id).distinct if opt == "artigo"
		a.each do |b|
    	result << Admin::Category.find(b["admin_category_id"])
    end
    result
  end

private 

  def user_not_authorized 
    flash[:alert] = "Você não tem autorização para acessar este conteudo." 
    redirect_to(request.referrer || root_path) 
  end 

  def pundit_user 
    current_admin_user 
  end 

end
