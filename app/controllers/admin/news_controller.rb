class Admin::NewsController < ApplicationController
  before_action :set_admin_news, only: [:show, :edit, :update, :destroy]
 
  def index
    #@admin_news = Admin::News.all
    #@noticias_mais_pupulares = NoticiasHelper.mais_populares
  end

  def show
  end

  def new
    @admin_news = Admin::News.new
  end

  def edit
  end

  def create
    @admin_news = Admin::News.new(admin_news_params)
    #@admin_news.img_url.attach(params[:admin_news][:img_url])
    respond_to do |format|
      if @admin_news.save
        format.html { redirect_to @admin_news, notice: 'News was successfully created.' }
        format.json { render :show, status: :created, location: @admin_news }
      else
        format.html { render :new }
        format.json { render json: @admin_news.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @admin_news.update(admin_news_params)
        format.html { redirect_to @admin_news, notice: 'News was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_news }
      else
        format.html { render :edit }
        format.json { render json: @admin_news.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @admin_news.destroy
    respond_to do |format|
      format.html { redirect_to admin_news_index_url, notice: 'News was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_admin_news
      @admin_news = Admin::News.find(params[:id])
    end

    def admin_news_params
      params.require(:admin_news).permit(:admin_publisher_id, :title, :date, :subtitle, :text, :active,:image)
    end
end
