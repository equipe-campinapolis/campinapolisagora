class Admin::ShortUrlsController < ApplicationController
  before_action :set_admin_short_url, only: [:show, :edit, :update, :destroy]

  # GET /admin/short_urls
  # GET /admin/short_urls.json
  def index
    @admin_short_urls = Admin::ShortUrl.all
  end

  # GET /admin/short_urls/1
  # GET /admin/short_urls/1.json
  def show
  end

  # GET /admin/short_urls/new
  def new
    @admin_short_url = Admin::ShortUrl.new
  end

  # GET /admin/short_urls/1/edit
  def edit
  end

  # POST /admin/short_urls
  # POST /admin/short_urls.json
  def create
    @admin_short_url = Admin::ShortUrl.new(admin_short_url_params)

    respond_to do |format|
      if @admin_short_url.save
        format.html { redirect_to @admin_short_url, notice: 'Short url was successfully created.' }
        format.json { render :show, status: :created, location: @admin_short_url }
      else
        format.html { render :new }
        format.json { render json: @admin_short_url.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/short_urls/1
  # PATCH/PUT /admin/short_urls/1.json
  def update
    respond_to do |format|
      if @admin_short_url.update(admin_short_url_params)
        format.html { redirect_to @admin_short_url, notice: 'Short url was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_short_url }
      else
        format.html { render :edit }
        format.json { render json: @admin_short_url.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/short_urls/1
  # DELETE /admin/short_urls/1.json
  def destroy
    @admin_short_url.destroy
    respond_to do |format|
      format.html { redirect_to admin_short_urls_url, notice: 'Short url was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_short_url
      @admin_short_url = Admin::ShortUrl.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_short_url_params
      params.require(:admin_short_url).permit(:admin_news_id_id, :short_url_key)
    end
end
