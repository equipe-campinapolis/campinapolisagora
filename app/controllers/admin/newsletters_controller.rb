class Admin::NewslettersController < ApplicationController
  before_action :set_admin_newsletter, only: [:show, :edit, :update, :destroy]

  def index
    @admin_newsletters = Admin::Newsletter.all
  end


  # def show
  # end


  def new
    @admin_newsletter = Admin::Newsletter.new
  end


  # def edit
  # end


  def create
    if params[:admin_newsletter][:mail].blank?
      msg = {:erro => "Oops! Alguma coisa aconteceu e não podemos confirmar sua inscrição."}
      flash[:newsletter_confirmation] = msg
      redirect_to root_path
      return
    end
    @admin_newsletter = Admin::Newsletter.new(admin_newsletter_params)
    @admin_newsletter.confirmation_id = ApplicationHelper.newsletter_confirmacao_id
    respond_to do |format|
      if @admin_newsletter.save
        flash[:newsletter] = 'Enviamos um e-mail de confirmacao para você.'
        format.html { redirect_to root_path }
        #format.json { render :show, status: :created, location: @admin_newsletter }
      else
        format.html { render :new }
        #format.json { render json: @admin_newsletter.errors, status: :unprocessable_entity }
      end
    end
  end


  # def update
  #   respond_to do |format|
  #     if @admin_newsletter.update(admin_newsletter_params)
  #       format.html { redirect_to @admin_newsletter, notice: 'Newsletter was successfully updated.' }
  #       format.json { render :show, status: :ok, location: @admin_newsletter }
  #     else
  #       format.html { render :edit }
  #       format.json { render json: @admin_newsletter.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end


  # def destroy
  #   @admin_newsletter.destroy
  #   respond_to do |format|
  #     format.html { redirect_to admin_newsletters_url, notice: 'Newsletter was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  def confirmar_inscricao
    id = params.permit("id")["id"]
    boletim = Admin::Newsletter.where(confirmation_status: nil,confirmation_id: id).first
    flash[:newsletter_confirmation] = []
    if not boletim.nil?
      boletim.confirmation_status = true
      boletim.save
      msg = {:sucesso => "Obrigado por confirmar sua inscrição no nosso boletim! Você recberá toda semana nosso boletim em seu e-mail."}
    else
      msg = {:erro => "Oops! Alguma coisa aconteceu e não podemos confirmar sua inscrição."}
    end
    flash[:newsletter_confirmation] = msg
    redirect_to root_path
    return
  end

  private

    def set_admin_newsletter
      @admin_newsletter = Admin::Newsletter.find(params[:id])
    end


    def admin_newsletter_params
      params.require(:admin_newsletter).permit(:mail)
    end
end
