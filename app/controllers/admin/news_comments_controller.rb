class Admin::NewsCommentsController < ApplicationController
  before_action :set_admin_news_comment, only: [:show, :edit, :update, :destroy]
  
  #def index
  #  @admin_news_comments = Admin::NewsComment.all
  #end

  #def show
  #end

  def new
    @admin_news_comment = Admin::NewsComment.new
  end

  #def edit
  #end

  def create
    @admin_news_comment = Admin::NewsComment.new(admin_news_comment_params)
    respond_to do |format|
      if @admin_news_comment.save
        format.html { 
          redirect_to request.referrer, :flash => { :success => "comentario" }
        }
      else
        redirect_to request.referrer, :flash => { :error => "comentario" }
      end
    end
  end

  #def update
  #  respond_to do |format|
  #    if @admin_news_comment.update(admin_news_comment_params)
  #      format.html { redirect_to @admin_news_comment, notice: 'News comment was successfully updated.' }
  #      format.json { render :show, status: :ok, location: @admin_news_comment }
  #    else
  #      format.html { render :edit }
  #      format.json { render json: @admin_news_comment.errors, status: :unprocessable_entity }
  #    end
  #  end
  #end

  def destroy
    @admin_news_comment.destroy
    respond_to do |format|
      format.html { redirect_to admin_news_comments_url, notice: 'News comment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_admin_news_comment
      @admin_news_comment = Admin::NewsComment.find(params[:id])
    end

    def admin_news_comment_params
      params.require(:admin_news_comment).permit(:admin_news_id, :name, :email, :message,:uid)
    end
end
