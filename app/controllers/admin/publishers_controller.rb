class Admin::PublishersController < ApplicationController
  before_action :set_admin_publisher, only: [:show, :edit, :update, :destroy]

  def index
    @admin_publishers = Admin::Publisher.all
  end

  def show
  end

  def new
    @admin_publisher = Admin::Publisher.new
  end

  def edit
  end

  def create
    @admin_publisher = Admin::Publisher.new(admin_publisher_params)
    respond_to do |format|
      if @admin_publisher.save
        format.html { redirect_to @admin_publisher, notice: 'Publisher was successfully created.' }
        format.json { render :show, status: :created, location: @admin_publisher }
      else
        format.html { render :new }
        format.json { render json: @admin_publisher.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @admin_publisher.update(admin_publisher_params)
        format.html { redirect_to @admin_publisher, notice: 'Publisher was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_publisher }
      else
        format.html { render :edit }
        format.json { render json: @admin_publisher.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @admin_publisher.destroy
    respond_to do |format|
      format.html { redirect_to admin_publishers_url, notice: 'Publisher was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_admin_publisher
      @admin_publisher = Admin::Publisher.find(params[:id])
    end

    def admin_publisher_params
      params.require(:admin_publisher).permit(:admin_user_id, :name, :active)
    end
end
