class Admin::VideoSerializer < ActiveModel::Serializer
  attributes :id, :title, :date
end
