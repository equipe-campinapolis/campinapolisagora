class Admin::ShortUrlSerializer < ActiveModel::Serializer
  attributes :id, :short_url_key
  has_one :admin_news_id
end
