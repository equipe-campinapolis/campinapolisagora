module FacebookHelper
    def self.fbgraph(token)
        Koala::Facebook::API.new(token)
    end
          
    def self.get_object(token, id, args = {}, options = {}, &block)
        fbgraph(token).get_object(id, args, options, &block)
    end

    def self.is_valid_token?(token, id, args = {}, options = {}, &block)
        begin
            fbgraph(token).get_object(id, args, options, &block)
            rescue Koala::Facebook::APIError => exception
            if exception.fb_error_code == 190
                return false
            else
                return false
            end
        end
        return true
    end
end