module ApplicationHelper
  def bootstrap_class_for flash_type
    { success: "alert-success", error: "alert-danger", alert: "alert-warning", notice: "alert-info" }.stringify_keys[flash_type.to_s] || flash_type.to_s
  end

  def flash_messages(opts = {})
    flash.each do |msg_type, message|
      concat(content_tag(:div, message, class: "alert #{bootstrap_class_for(msg_type)}", role: "alert") do
              concat content_tag(:button, 'x', class: "close", data: { dismiss: 'alert' })
              concat message
            end)
    end
    nil
  end

  def md5(text)
    require 'digest/md5'
    Digest::MD5.hexdigest(text)
  end


  def self.newsletter_confirmacao_id
    return (0...50).map { ('a'..'z').to_a[rand(26)] }.join
  end

  def converte_data_string(s)
    s = s.downcase
    result = "august" if not s["agosto"].nil?
    result = "september" if not s["setembro"].nil?
    return result
  end

end
