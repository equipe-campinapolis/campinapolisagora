module NoticiasHelper

  def noticias_do_slider
    noticias = Admin::News.slider.limit(6)
  end

  def self.noticias_recentes
    noticias = Admin::News.all.order(date: :DESC)
    un = NoticiasHelper.ultima_noticia
    uns = NoticiasHelper.ultimas_noticias
    ml = NoticiasHelper.noticia_mais_lida
    nml = NoticiasHelper.mais_lidas
    result = noticias - uns - nml
    remover = un
    result = result.reject{|noticia| noticia == remover}
    remover = ml
    result = result.reject{|noticia| noticia == remover}
    result.take(6).sort_by(&:created_at).reverse
  end

  def self.noticia_mais_lida
      mais_lidas = Admin::News.most_hit(365.day.ago,Admin::News.count)
      temp_ultima_noticia = NoticiasHelper.ultima_noticia 
      remover = temp_ultima_noticia
      result = mais_lidas.reject{|noticia| noticia == remover}
      result[0]
  end
  
  def self.noticias_mega_menu
    Admin::News.noticias_mega_menu
  end

  
  def self.mais_lidas
      mais_lidas = Admin::News.most_hit(365.day.ago,Admin::News.count)
      remover = NoticiasHelper.noticia_mais_lida
      temp_mais_lidas = mais_lidas.reject{|noticia| noticia == remover}
      ultimas_noticias = Admin::News.ultimas_noticias
      result = temp_mais_lidas - ultimas_noticias
      result.take(5)
  end

  def self.ultima_noticia
    ultima_noticia = Admin::News.ultimas_noticias
    ultima_noticia.first
  end

  def self.noticias_em_destaque
    Admin::News.mais_lidas
  end

  def self.ultimas_noticias
    ultima_noticia = self.ultima_noticia
    Admin::News.ultimas_noticias.reject{|noticia| noticia == ultima_noticia}
  end

  def noticias_arquivos
    Admin::News.all
  end

  def cinco_mais_lidas
    Admin::News.mais_lidas
  end

  def noticias_cinco_aleatorias
    cinco = []
    cinco << Admin::News.all.sample
    cinco << Admin::News.all.sample
    cinco << Admin::News.all.sample
    cinco << Admin::News.all.sample
    cinco << Admin::News.all.sample
    cinco
  end

  def agrupar_noticias
    Admin::News.all.group_by { |m| m.created_at.beginning_of_month }
  end


end
