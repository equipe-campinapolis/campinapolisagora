module EventosHelper
    
    def evento_principal (arg)
        result = Admin::Event.last if arg[:id].to_i.is_a? Integer
        result
    end

    def listar_eventos
        Admin::Event.all
    end

    def listar_recentes
        Admin::Event.all
    end
    
    def listar_antigos
        Admin::Event.all
    end

    def listar_muito_antigos
        Admin::Event.all
    end
end
