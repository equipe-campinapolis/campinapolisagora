class CreateAdminAdvertisements < ActiveRecord::Migration[5.2]
  def change
    create_table :admin_advertisements do |t|
      t.references :admin_publisher, foreign_key: true
      t.string :customer
      t.datetime :expiration

      t.timestamps
    end
  end
end
