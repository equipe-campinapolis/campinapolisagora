class AddYoutubeUrlToAdminVideos < ActiveRecord::Migration[5.2]
  def change
    add_column :admin_videos, :youtube_url, :string
  end
end
