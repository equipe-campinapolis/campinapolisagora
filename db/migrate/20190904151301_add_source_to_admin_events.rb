class AddSourceToAdminEvents < ActiveRecord::Migration[5.2]
  def change
    add_column :admin_events, :source, :string
    add_column :admin_events, :url, :string
  end
end
