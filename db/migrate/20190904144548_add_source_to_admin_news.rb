class AddSourceToAdminNews < ActiveRecord::Migration[5.2]
  def change
    add_column :admin_news, :source, :string
    add_column :admin_news, :url, :string
  end
end
