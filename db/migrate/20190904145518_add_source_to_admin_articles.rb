class AddSourceToAdminArticles < ActiveRecord::Migration[5.2]
  def change
    add_column :admin_articles, :source, :string
    add_column :admin_articles, :url, :string
  end
end
