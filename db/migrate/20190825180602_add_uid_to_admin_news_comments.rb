class AddUidToAdminNewsComments < ActiveRecord::Migration[5.2]
  def change
    add_column :admin_news_comments, :uid, :string
  end
end
