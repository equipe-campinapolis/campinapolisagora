class CreateAdminGalleries < ActiveRecord::Migration[5.2]
  def change
    create_table :admin_galleries do |t|
      t.timestamps
    end
  end
end
