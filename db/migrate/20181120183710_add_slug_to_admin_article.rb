class AddSlugToAdminArticle < ActiveRecord::Migration[5.2]
  def change
    add_column :admin_articles, :slug, :string
  end
end
