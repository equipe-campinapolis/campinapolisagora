class CreateAdminNewsComments < ActiveRecord::Migration[5.2]
  def change
    create_table :admin_news_comments do |t|
      t.references :admin_news, foreign_key: true
      t.string :name
      t.string :email
      t.text :message

      t.timestamps
    end
  end
end
