class CreateAdminSurveys < ActiveRecord::Migration[5.2]
  def change
    create_table :admin_surveys do |t|
      t.string :title
      t.integer :survey_id
      t.boolean :active

      t.timestamps
    end
  end
end
