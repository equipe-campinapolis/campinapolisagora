class CreateAdminPublishers < ActiveRecord::Migration[5.2]
  def change
    create_table :admin_publishers do |t|
      t.references :admin_user, foreign_key: true
      t.string :name
      t.boolean :active

      t.timestamps
    end
  end
end
