class AddSlugToAdminPublisher < ActiveRecord::Migration[5.2]
  def change
    add_column :admin_publishers, :slug, :string
  end
end
