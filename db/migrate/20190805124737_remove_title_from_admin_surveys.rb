class RemoveTitleFromAdminSurveys < ActiveRecord::Migration[5.2]
  def self.up
    remove_column :admin_surveys, :title 
  end
end
