class AddSlugToAdminCategory < ActiveRecord::Migration[5.2]
  def change
    add_column :admin_categories, :slug, :string
  end
end
