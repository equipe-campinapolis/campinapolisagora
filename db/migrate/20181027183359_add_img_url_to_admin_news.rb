class AddImgUrlToAdminNews < ActiveRecord::Migration[5.2]
  def change
    add_column :admin_news, :img_url, :string
  end
end
