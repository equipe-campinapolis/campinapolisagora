class CreateAdminNewsletters < ActiveRecord::Migration[5.2]
  def change
    create_table :admin_newsletters do |t|
      t.string :mail

      t.timestamps
    end
  end
end
