class RemoveTagsFromAticles < ActiveRecord::Migration[5.2]
  def change
  	remove_column :admin_articles, :tags
  end
end
