class CreateAdminArticles < ActiveRecord::Migration[5.2]
  def change
    create_table :admin_articles do |t|
      t.references :admin_category, foreign_key: true
      t.references :admin_publisher, foreign_key: true
      t.string :title
      t.string :subtitle
      t.text :article
      t.datetime :date
      t.boolean :active
      t.string :tags

      t.timestamps
    end
  end
end
