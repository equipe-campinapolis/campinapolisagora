class FixUserIsAdmin < ActiveRecord::Migration[5.2] 
def change 
    change_column :admin_users, :is_admin, 'integer USING CAST(is_admin AS integer)'  
  end 

end
