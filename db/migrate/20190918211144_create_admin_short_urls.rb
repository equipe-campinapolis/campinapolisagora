class CreateAdminShortUrls < ActiveRecord::Migration[5.2]
  def change
    create_table :admin_short_urls do |t|
      t.references :admin_news, foreign_key: true
      t.string :short_url_key
      t.timestamps
    end
  end
end
