class AddLinkToAdminAdvertisement < ActiveRecord::Migration[5.2]
  def change
    add_column :admin_advertisements, :link_status, :integer
    add_column :admin_advertisements, :link, :string
  end
end
