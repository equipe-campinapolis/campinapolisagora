class CreateAdminContacts < ActiveRecord::Migration[5.2]
  def change
    create_table :admin_contacts do |t|
      t.string :name
      t.string :second_name
      t.string :email
      t.string :social
      t.text :message

      t.timestamps
    end
  end
end
