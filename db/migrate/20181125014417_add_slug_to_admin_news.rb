class AddSlugToAdminNews < ActiveRecord::Migration[5.2]
  def change
    add_column :admin_news, :slug, :string
  end
end
