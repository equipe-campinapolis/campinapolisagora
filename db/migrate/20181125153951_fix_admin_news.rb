class FixAdminNews < ActiveRecord::Migration[5.2]
  def up
    change_column :admin_news, :date, :datetime
  end

  def down
    change_column :admin_news, :date, :date
  end
end
