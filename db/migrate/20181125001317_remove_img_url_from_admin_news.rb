class RemoveImgUrlFromAdminNews < ActiveRecord::Migration[5.2]
  def self.up  
    remove_column :admin_news, :img_url 
  end 
end
