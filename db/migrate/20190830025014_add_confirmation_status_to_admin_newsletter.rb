class AddConfirmationStatusToAdminNewsletter < ActiveRecord::Migration[5.2]
  def change
    add_column :admin_newsletters, :confirmation_status, :boolean
  end
end
