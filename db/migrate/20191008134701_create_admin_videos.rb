class CreateAdminVideos < ActiveRecord::Migration[5.2]
  def change
    create_table :admin_videos do |t|
      t.string :title
      t.date :date
      t.timestamps
    end
  end
end
