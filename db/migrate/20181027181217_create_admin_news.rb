class CreateAdminNews < ActiveRecord::Migration[5.2]
  def change
    create_table :admin_news do |t|
      t.references :admin_publisher, foreign_key: true
      t.string :title
      t.date :date
      t.string :subtitle
      t.string :text
      t.boolean :active

      t.timestamps
    end
  end
end
