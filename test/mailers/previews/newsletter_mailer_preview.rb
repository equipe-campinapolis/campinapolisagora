# Preview all emails at http://localhost:3000/rails/mailers/artigos_mailer
class NewsletterMailerPreview < ActionMailer::Preview
  # Preview this email at http://localhost:3000/rails/mailers/rental_mailer/confirmation
  def newsletter
    NewsletterMailer.newsletter
  end
end