require "application_system_test_case"

class Admin::NewslettersTest < ApplicationSystemTestCase
  setup do
    @admin_newsletter = admin_newsletters(:one)
  end

  test "visiting the index" do
    visit admin_newsletters_url
    assert_selector "h1", text: "Admin/Newsletters"
  end

  test "creating a Newsletter" do
    visit admin_newsletters_url
    click_on "New Admin/Newsletter"

    fill_in "Mail", with: @admin_newsletter.mail
    click_on "Create Newsletter"

    assert_text "Newsletter was successfully created"
    click_on "Back"
  end

  test "updating a Newsletter" do
    visit admin_newsletters_url
    click_on "Edit", match: :first

    fill_in "Mail", with: @admin_newsletter.mail
    click_on "Update Newsletter"

    assert_text "Newsletter was successfully updated"
    click_on "Back"
  end

  test "destroying a Newsletter" do
    visit admin_newsletters_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Newsletter was successfully destroyed"
  end
end
