require "application_system_test_case"

class Admin::NewsCommentsTest < ApplicationSystemTestCase
  setup do
    @admin_news_comment = admin_news_comments(:one)
  end

  test "visiting the index" do
    visit admin_news_comments_url
    assert_selector "h1", text: "Admin/News Comments"
  end

  test "creating a News comment" do
    visit admin_news_comments_url
    click_on "New Admin/News Comment"

    fill_in "Admin News", with: @admin_news_comment.admin_news_id
    fill_in "Email", with: @admin_news_comment.email
    fill_in "Message", with: @admin_news_comment.message
    fill_in "Name", with: @admin_news_comment.name
    click_on "Create News comment"

    assert_text "News comment was successfully created"
    click_on "Back"
  end

  test "updating a News comment" do
    visit admin_news_comments_url
    click_on "Edit", match: :first

    fill_in "Admin News", with: @admin_news_comment.admin_news_id
    fill_in "Email", with: @admin_news_comment.email
    fill_in "Message", with: @admin_news_comment.message
    fill_in "Name", with: @admin_news_comment.name
    click_on "Update News comment"

    assert_text "News comment was successfully updated"
    click_on "Back"
  end

  test "destroying a News comment" do
    visit admin_news_comments_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "News comment was successfully destroyed"
  end
end
