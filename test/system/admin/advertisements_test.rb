require "application_system_test_case"

class Admin::AdvertisementsTest < ApplicationSystemTestCase
  setup do
    @admin_advertisement = admin_advertisements(:one)
  end

  test "visiting the index" do
    visit admin_advertisements_url
    assert_selector "h1", text: "Admin/Advertisements"
  end

  test "creating a Advertisement" do
    visit admin_advertisements_url
    click_on "New Admin/Advertisement"

    fill_in "Admin Publisher", with: @admin_advertisement.admin_publisher_id
    fill_in "Customer", with: @admin_advertisement.customer
    fill_in "Expiration", with: @admin_advertisement.expiration
    click_on "Create Advertisement"

    assert_text "Advertisement was successfully created"
    click_on "Back"
  end

  test "updating a Advertisement" do
    visit admin_advertisements_url
    click_on "Edit", match: :first

    fill_in "Admin Publisher", with: @admin_advertisement.admin_publisher_id
    fill_in "Customer", with: @admin_advertisement.customer
    fill_in "Expiration", with: @admin_advertisement.expiration
    click_on "Update Advertisement"

    assert_text "Advertisement was successfully updated"
    click_on "Back"
  end

  test "destroying a Advertisement" do
    visit admin_advertisements_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Advertisement was successfully destroyed"
  end
end
