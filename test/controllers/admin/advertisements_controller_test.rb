require 'test_helper'

class Admin::AdvertisementsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin_advertisement = admin_advertisements(:one)
  end

  test "should get index" do
    get admin_advertisements_url
    assert_response :success
  end

  test "should get new" do
    get new_admin_advertisement_url
    assert_response :success
  end

  test "should create admin_advertisement" do
    assert_difference('Admin::Advertisement.count') do
      post admin_advertisements_url, params: { admin_advertisement: { admin_publisher_id: @admin_advertisement.admin_publisher_id, customer: @admin_advertisement.customer, expiration: @admin_advertisement.expiration } }
    end

    assert_redirected_to admin_advertisement_url(Admin::Advertisement.last)
  end

  test "should show admin_advertisement" do
    get admin_advertisement_url(@admin_advertisement)
    assert_response :success
  end

  test "should get edit" do
    get edit_admin_advertisement_url(@admin_advertisement)
    assert_response :success
  end

  test "should update admin_advertisement" do
    patch admin_advertisement_url(@admin_advertisement), params: { admin_advertisement: { admin_publisher_id: @admin_advertisement.admin_publisher_id, customer: @admin_advertisement.customer, expiration: @admin_advertisement.expiration } }
    assert_redirected_to admin_advertisement_url(@admin_advertisement)
  end

  test "should destroy admin_advertisement" do
    assert_difference('Admin::Advertisement.count', -1) do
      delete admin_advertisement_url(@admin_advertisement)
    end

    assert_redirected_to admin_advertisements_url
  end
end
