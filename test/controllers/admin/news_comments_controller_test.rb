require 'test_helper'

class Admin::NewsCommentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin_news_comment = admin_news_comments(:one)
  end

  test "should get index" do
    get admin_news_comments_url
    assert_response :success
  end

  test "should get new" do
    get new_admin_news_comment_url
    assert_response :success
  end

  test "should create admin_news_comment" do
    assert_difference('Admin::NewsComment.count') do
      post admin_news_comments_url, params: { admin_news_comment: { admin_news_id: @admin_news_comment.admin_news_id, email: @admin_news_comment.email, message: @admin_news_comment.message, name: @admin_news_comment.name } }
    end

    assert_redirected_to admin_news_comment_url(Admin::NewsComment.last)
  end

  test "should show admin_news_comment" do
    get admin_news_comment_url(@admin_news_comment)
    assert_response :success
  end

  test "should get edit" do
    get edit_admin_news_comment_url(@admin_news_comment)
    assert_response :success
  end

  test "should update admin_news_comment" do
    patch admin_news_comment_url(@admin_news_comment), params: { admin_news_comment: { admin_news_id: @admin_news_comment.admin_news_id, email: @admin_news_comment.email, message: @admin_news_comment.message, name: @admin_news_comment.name } }
    assert_redirected_to admin_news_comment_url(@admin_news_comment)
  end

  test "should destroy admin_news_comment" do
    assert_difference('Admin::NewsComment.count', -1) do
      delete admin_news_comment_url(@admin_news_comment)
    end

    assert_redirected_to admin_news_comments_url
  end
end
