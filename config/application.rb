require_relative 'boot'
require 'rails/all'
Bundler.require(*Rails.groups)

module Cmpagora
  class Application < Rails::Application
    require Rails.root.join("lib/custom_public_exceptions") 
    config.load_defaults 5.2
    #I18n.available_locales = [:en, :'pt-BR']
    #config.i18n.default_locale = 'pt-BR'
    I18n.enforce_available_locales = true
    config.i18n.available_locales = 'pt-BR'
    config.time_zone = "Brasilia"
    config.active_record.default_timezone = :local
    config.exceptions_app = CustomPublicExceptions.new(Rails.public_path) 
  end
end
