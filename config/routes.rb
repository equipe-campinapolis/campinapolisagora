Rails.application.routes.draw do
  root to: "inicio#index"
  mount RailsAdmin::Engine => '/site/admin', as: 'rails_admin'
  mount Rapidfire::Engine => "/enquete"
  mount Ckeditor::Engine => '/ckeditor'
  #administração
  namespace :admin do
      resources :categories
      resources :news
      resources :publishers
      resources :articles
      resources :galleries
      resources :news_comments
      resources :advertisements
      resources :events
      resources :videos
      resources :contacts
      resources :surveys
      resources :newsletters
      resources :short_urls
      devise_for :users, class_name: "Admin::User",controllers:{
                                                   sessions: 'admin/users/sessions',
                                                   registrations: 'admin/users/registrations',
                                                   passwords: 'admin/users/passwords',
                                                   omniauth_callbacks:'admin/users/omniauth_callbacks' 
  }
  end
  #inicio
  get "/inicio" => "inicio#index"
  #categoria
  get "/categoria/:categoria/:id", to: "categoria#index"
  get "/categoria/arquivos/:id", to: "categoria#index"
  #noticia
  get "/noticias" => "noticia#index"
  get "/noticia/:id" => "noticia#index"
  #artigo
  get "/artigo/:id" => "artigo#index"
  get "/artigo/autor/:id" => "artigo#index_autor"
  get "/galeria" => "galeria#index"
  #eventos
  get '/eventos' => 'evento#index'
  get '/evento/:id' => 'evento#index'
  #pesquisa
  get "/pesquisa" => "pesquisa#index"
  get "/pesquisa/ultimas_noticias" => "pesquisa#ultimas_noticias"
  get "/pesquisa/todas_as_noticias" => "pesquisa#todas_as_noticias"
  get "/pesquisa/mais_lidas" => "pesquisa#mais_lidas"
  get "/pesquisa/eventos" => "pesquisa#eventos"
  get "/pesquisa/videos" => "pesquisa#videos"
  #outros
  get "/sobre" => "sobre#index"
  get "/contato" => "contato#index"
  get "/videos" => "video#index"
  get "/boletim/:id" => "admin/newsletters#confirmar_inscricao"
  get 'whatsapp/ultima_noticia' => "whatsapp#ultima_noticia"
  get 'whatsapp/noticia/:id' => "whatsapp#noticia"
  #404
  match "/404" => "errors#error404", via: [ :get, :post, :patch, :delete ] 
end
