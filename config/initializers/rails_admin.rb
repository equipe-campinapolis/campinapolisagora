include RailsAdminTagList::SuggestionsHelper
require 'i18n'
I18n.default_locale = :'pt-BR'
RailsAdmin.config do |config|
  config.main_app_name = ["CAMPINAPOLIS AGORA", " - ADMINISTRAÇÃO"]
  #config.included_models << "ActsAsTaggableOn::Tag"
  #config.included_models << "ActsAsTaggableOn::Tagging"
  config.excluded_models << "Ckeditor::Asset"
  config.excluded_models << "Ckeditor::AttachmentFile"
  config.excluded_models << "Ckeditor::Picture"
  config.included_models << "Admin::Article"
  config.included_models << "Admin::Video"
  config.included_models << "Admin::Publisher"
  config.included_models << "Admin::User"
  config.included_models << "Admin::News"
  config.included_models << "Admin::Category"
  config.included_models << "Admin::Advertisement"
  config.included_models << "Admin::Event"
  config.included_models << "Admin::Gallery"
  config.included_models << "Admin::Survey"

  config.parent_controller = '::ApplicationController'
  config.current_user_method(&:current_admin_user)
  config.authenticate_with do
    warden.authenticate! scope: :admin_user 
  end

  ActiveRecord::Base.descendants.each do |imodel|
    config.model "#{imodel.name}" do
      list do
        exclude_fields :created_at, :updated_at
      end
    end
  end

  config.model 'Admin::Advertisement' do
    label "Comercial"
    label_plural "Comerciais"
    edit do
      configure :customer do
        label "Cliente"
      end
      configure :admin_publisher do
        label "Publicado por"
      end
      configure :expiration do
        label "Expira em"
      end
      configure :image do
        label "Imagem"
      end
      configure :link_status do
        label "Possui link"
      end
      configure :link do
        label "Link"
      end
    end
  end

  config.model 'Admin::Event' do
    label "Evento"
    label_plural "Eventos"
    edit do
      configure :source do
        label "Fonte"
      end
      configure :url do
        label "endereço da fonte"
      end
    end
  end

  config.model 'Admin::Survey' do
    label "Enquete"
    label_plural "Enquetes"
    edit do
      configure :active do
        label "Visivil"
      end
      configure :survey_id, :enum do
        label "Enquete"
        enum do
          Rapidfire::Survey.all.collect{|c| [c.name, c.id]}
        end
      end
    end
  end

  config.model 'Admin::Gallery' do
    label "Galeria"
    label_plural "Galerias"
    edit do
      #configure :link do
      #  label "Link"
      #end
    end
  end

  config.model 'Admin::Category' do
    edit do
      configure :name do
        label "Nome"
      end
      configure :active do
        label "Visivel"
      end
      configure :slug do
        hide
      end
    end
  end

  config.model 'Admin::Publisher' do
    edit do
      configure :admin_user do
        label "Nome do Usuário"
      end
      configure :name do
        label "Nome"
      end
      configure :active do
        label "Visivel"
      end
      configure :slug do
        hide
      end
    end
  end

  config.model 'Admin::Article' do
    label "Artigo"
    label_plural "Artigos"
    field :admin_publisher do
      label 'Autor'
    end
    field :title do
      label 'Titulo'
    end
    field :date do
      label 'Data'
      strftime_format '%d/%m/%Y'
    end
    edit do
      field :article, :ck_editor
      include_all_fields
      configure :article do
        label 'Artigo: '
      end
      configure :admin_category do
        label 'Categoria'
      end
       configure :admin_publisher do
        label 'Autor'
      end
      configure :title do
        label 'Titulo'
      end
       configure :subtitle do
        label 'Subtitulo'
      end
       configure :date do
        label 'Data'
      end
      configure :active do
        label 'Visivel'
      end
      configure :slug do
        hide
      end
      configure :source do
        label "Fonte"
      end
      configure :url do
        label "endereço da fonte"
      end
      fields_of_type :tag_list do
        partial 'tag_list_with_suggestions'
        ratl_max_suggestions -1
        label 'Descritores'
      end
    end
  end

  config.model 'Admin::Category' do
    label "Categoria"
    label_plural "Categorias"
  end

  config.model 'Admin::Publisher' do
    label "Autor"
    label_plural "Autores"
  end

  config.model 'Admin::User' do
    label "Usuario"
    label_plural "Usuarios"
  end



  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app
    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end

end


